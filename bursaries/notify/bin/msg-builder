#!/bin/bash -eu

# Copyright © 2014  Felipe Augusto van de Wiel <faw@funlabs.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# On Debian systems, you can find the full text of the license in
# /usr/share/common-licenses/GPL-2

# This script will iterate thru a CSV file and using a template will
# generate invidual email messages (hint: you can use dispatcher to
# send the messages)

set -eu
export LC_ALL=C

usage() {
  echo "Usage: $0 <list.csv> <file.template> <message-dir>" >&2
  echo "Build invidual messages to be used by mutt to send emails" >&2
  echo "" >&2
  echo "  <list.csv>      file containing pre-formatted data (CSV)" >&2
  echo "  <template>	  file used as template to create the messages" >&2
  echo "  <message-dir>   directory where the messages will be generated" >&2
  echo "" >&2
}

if [ $# -ne 3 ]; then
  echo "Error: invalid number of arguments!" >&2
  usage
  exit 1
else
  BASEDIR=$(dirname $(dirname $(readlink -f $0)))
  COMMON_CONFIG="${BASEDIR}/etc/common-config"
  if [ ! -f "${COMMON_CONFIG}" ]; then
    echo "Error: ${COMMON_CONFIG} not found!" >&2
    exit 1
  else
    . "${BASEDIR}/etc/common-config"
  fi

  LIST=$(readlink -f "$1")
  TMPL=$(readlink -f "$2")
  MSGTOPDIR=$(readlink -f "$3")
  TIMESTAMP=$(date +%Y%m%d)
fi

if [ ! -f "$LIST" ]; then
  echo "Error: ${LIST} is not a regular file?" >&2
  usage
  exit 1
fi

if [ ! -f "${TMPL}" ]; then
  echo "Error: ${TMPL} is not a regular file?" >&2
  usage
  exit 1
else
  MSGTPLDIR=$(basename "${TMPL}" .template)
fi

MSGDIR="${MSGTOPDIR}/${MSGTPLDIR}"
if [ ! -d "${MSGDIR}" ]; then
  mkdir -p "${MSGDIR}"
fi
MSGTMPDIR=$(mktemp -d "${MSGDIR}/msgdir.${TIMESTAMP}.XXXXXX")

OLDIFS=${IFS}
IFS='
'

COUNT=0
for i in $(cat "${LIST}"); do
	DC_NAME=$(echo "${i}" |cut -d, -f"${CSV_FIELD_NAME}")
	DC_MAIL=$(echo "${i}" |cut -d, -f"${CSV_FIELD_MAIL}")
	DC_ACCT=$(echo "${i}" |cut -d, -f"${CSV_FIELD_ACCT}")
	DC_COST=$(echo "${i}" |cut -d, -f"${CSV_FIELD_COST}")
	DC_NEED=$(echo "${i}" |cut -d, -f"${CSV_FIELD_NEED}")

	echo -n "Generating ${DC_ACCT}... "
	cat "${TMPL}" | \
		sed "s/DEBCONF_FULLNAME/${DC_NAME}/g" | \
		sed "s/DEBCONF_EDITION/${DC_EDITION}/g" | \
		sed "s/DEBCONF_LOCATION/${DC_LOCATION}/g" | \
		sed "s/DEBCONF_COSTS/${DC_COST}/g" | \
		sed "s/DEBCONF_NEEDED/${DC_NEED}/g" | \
		sed "s/DEBCONF_DONATION_URL/${DC_DONATE}/g" | \
		sed "s/DEBCONF_SIGNATURE/${DC_SIGNATURE}/g" \
		> "${MSGTMPDIR}/${DC_MAIL}.msg"
	echo "done."
	COUNT=$((COUNT+1))
done

echo "${COUNT} messages generated in ${MSGDIR}"

IFS=${OLDIFS}
exit 0

