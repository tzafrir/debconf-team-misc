#!/bin/sh
# create-repo.sh
#
# Shell script to facilitate creation of Git repos for
# debconf-team/debconf-data.
#
# Symlink this script into /git/debconf-{team,data} and run from there:
#
#   /git/debconf-team/create-repo reponame description text for gitweb etc.
#
# © 2014 martin f. krafft <madduck@debian.org>
# Released under the terms of the Artistic Licence 2.0
#
set -eu

if [ $# -lt 2 ]; then
  echo >&2 "E: Usage: $0 reponame text to use for description"
  exit 1
fi

REPO="${1%.git}.git"; shift
DESCRIPTION="$@"

cd ${0%/*}

if [ -d "$REPO" ]; then
  echo >&2 "E: repo '$REPO' already exists..."
  exit 2
fi

case "$PWD" in
  (*/debconf-team) group=debconf-team; shared=group ;;
  (*/debconf-data) group=debconf-data; shared=all ;;
  (*)
    echo >&2 "E: do not know how to run $0 in $PWD"
    exit 3
    ;;
esac

GIT_DIR="$REPO" git --bare init --shared=$shared
cd "$REPO"
git config --remove-section receive
printf "$DESCRIPTION\n" > description
chgrp -R scm_${group} .
find -type d -exec chmod g+s {} +
setfacl -R -m group:scm_${group}:rwX,default:group:scm_${group}:rwX,default:mask::rwX,default:group::rwX .
[ $shared = all ] && mv hooks/post-update.sample hooks/post-update
ln -s /home/groups/debconf-team/misc/git-repos/run-multiple-post-receive-hooks.sh hooks/post-receive
mkdir hooks/post-receive.d
ln -s /home/groups/debconf-team/kgb-client/hooks/git-post-receive hooks/post-receive.d/kgb-client.hook

if [ $group = debconf-data ]; then
  ln -s /home/groups/debconf-team/misc/git-repos/dc-website-updating/poke-dc-webserver.sh hooks/post-receive.d/poke-dc-webserver.hook
fi

echo >&2 "Done."
echo "git clone ssh://git.debian.org${PWD}"
echo >&2 "Please make sure to add the repo appropriately to the .mrconfig files"
echo >&2 "in the debconf-team and debconf-data repositories!"
