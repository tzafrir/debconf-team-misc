#!/bin/sh
# poke-dc-webserver.sh
#
# Git post-receive hook to poke the dc.o webserver to trigger a website
# update.
#
# © 2014 martin f. krafft <madduck@debian.org>
# Released under the terms of the Artistic Licence 2.0
#
set -eu

export LC_ALL=C.UTF-8

tmpfile=$(tempfile -p dcpoke -s .key)
cleanup() { rm -f $tmpfile; trap - 1 2 3 4 5 6 7 8 10 11 12 13 14 15; }
trap cleanup 1 2 3 4 5 6 7 8 10 11 12 13 14 15

poke_webserver() {
  local sshserver; sshserver="$1"
  local sshport; sshport="$2"
  local repo; repo="$3"
  local subdir; subdir="$4"
  local ref; ref="$5"
  local old; old="$6"
  local new; new="$7"

  echo "Poking $sshserver:$sshport to update site corresponding to $repo/$subdir..." >&2

  dc_website_update_base=$(readlink -f $0)
  dc_website_update_base=${dc_website_update_base%/*}

  cat ${dc_website_update_base}/ssh-key_ecdsa > $tmpfile

  ssh -o Port="$sshport" -o ConnectTimeout=10 \
    -o StrictHostKeyChecking=true \
    -o ForwardAgent=no -o ForwardX11=no \
    -o PreferredAuthentications=publickey \
    -o IdentitiesOnly=yes \
    -o UserKnownHostsFile="${dc_website_update_base}/ssh_known_hosts" \
    -o IdentityFile="$tmpfile" \
    "$sshserver" \
    "update $repo $subdir $ref $old $new"
}

while read old new ref; do

  case "$ref" in
    (refs/tags/*) continue;;
    (*$(git config --get dc-website.reflimit)) :;;
    (*) continue;;
  esac

  sshserver=$(git config --get dc-website.sshserver) || sshserver=web@kent.debconf.org
  sshport=$(git config --get dc-website.sshport) || sshport=22
  repo=$(git config --get dc-website.repo) || repo="${PWD##*/git/}"
  subdir=$(git config --get dc-website.subdir) || subdir=website

  if [ $subdir = . ] || git diff --name-only ${old}..${new} | grep -q "^${subdir}/"; then
    poke_webserver "$sshserver" "$sshport" "$repo" "$subdir" "$ref" "$old" "$new"
  fi

done

cleanup
