#!/bin/sh
# run-multiple-post-receive-hooks.sh
#
# Shell glue to run multiple (post-receive) hooks.
#
# Symlink this into $GIT_DIR/hooks/post-receive and then install your
# hooks into $GIT_DIR/hooks/post-receive.d/*.hook
#
# © 2014 martin f. krafft <madduck@debian.org>
# Released under the terms of the Artistic Licence 2.0
#
set -eu

SELF="${GIT_DIR}/$0"
PARTS_DIR="${SELF}.d"

# from http://zgp.org/~dmarti/tips/git-multiple-post-receive-hooks/
# pee is part of moreutils
exec pee "$PARTS_DIR"/*.hook
