1
00:00:02,740 --> 00:00:07,440
Hi, I’m Andrej, and I’m going to tell
you about DebConf in Bratislava.

2
00:00:07,440 --> 00:00:12,280
I presented this idea very roughly at DebConf
in Montréal last year, so I’m going to

3
00:00:12,280 --> 00:00:15,520
shorten up the introduction a bit.

4
00:00:15,520 --> 00:00:18,780
So, why have DebConf in Bratislava?

5
00:00:18,780 --> 00:00:25,340
After having so many DebConfs all over the globe,
this may be our first European DebConf.

6
00:00:25,340 --> 00:00:31,140
Bratislava is a small but nice city, well
connected, part of the EU.

7
00:00:31,140 --> 00:00:35,920
Good food and beer, plenty
of attractions around.

8
00:00:42,800 --> 00:00:49,060
Let's talk about progress.
We've got two primary venue candidates, both

9
00:00:49,060 --> 00:00:54,600
located at the same university campus close
to the city centre, really close to each other.

10
00:00:54,600 --> 00:01:02,460
The first one is a recently built information
technology faculty called FIIT, modern,

11
00:01:02,460 --> 00:01:07,640
with good equipment.
The management are willing to host DebConf,

12
00:01:07,640 --> 00:01:15,499
but it’s a bit expensive, and the cafeteria
isn’t big enough to accommodate all participants,

13
00:01:15,500 --> 00:01:22,920
so we may need to use
facilities of other faculties.

14
00:01:22,920 --> 00:01:32,420
The next candidate is Matfyz, a faculty of
a different university, much older, but because

15
00:01:32,420 --> 00:01:39,369
of this we may get it for free. The management
is friendly to Linux conferences, they've

16
00:01:39,369 --> 00:01:47,600
hosted some, there are some accessibility
issues, which I believe are solvable, and

17
00:01:47,600 --> 00:01:55,039
we may need to check the technical equipment
is suitable for our needs. The downside is

18
00:01:55,039 --> 00:02:00,590
that renovation may start next year, but so
far there's been no news.

19
00:02:00,590 --> 00:02:05,609
I will be meeting the faculty management shortly
to find out more.

20
00:02:05,609 --> 00:02:10,500
The campus belongs to two universities, there
are multiple faculties, so naturally there's

21
00:02:10,500 --> 00:02:17,671
student accommodation very close to the potential
venues. There are three options of accommodation,

22
00:02:17,671 --> 00:02:24,500
the accommodation is very cheap, because during
the summer most of the students move out,

23
00:02:24,500 --> 00:02:30,970
so they are happy to offer rooms to external
people.

24
00:02:30,970 --> 00:02:38,930
Here are some ideas for the day trip.

25
00:02:38,930 --> 00:02:47,590
Now, risks.
If Matfyz closes for renovation, we'll need

26
00:02:47,590 --> 00:02:55,000
to deal with FIIT, the bill may be too high
for the value we’re getting. Hopefully not.

27
00:02:55,000 --> 00:03:00,610
The next issue is the team. A couple of people
signed up for the core team, and there’s

28
00:03:00,610 --> 00:03:06,440
an organisation which also may help us, but
the people who signed up haven’t been active

29
00:03:06,440 --> 00:03:13,070
yet, so I need to make sure they engage more.
I’ve tried organising local Debian meetups,

30
00:03:13,070 --> 00:03:16,850
I hope I can get more people involved that
way.

31
00:03:16,850 --> 00:03:23,100
Hopefully, there will be a Mini-DebConf in
Bratislava, so we can test the venue and grow

32
00:03:23,100 --> 00:03:24,100
the team.

33
00:03:24,100 --> 00:03:28,290
That's all.
Thanks for watching, and — see you in Bratislava!

